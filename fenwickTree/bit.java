class FenwickTree {
    
    public int getPrefixSum(int[] tree, int index) {
        index += 1;
        int sum = 0;
        while (index > 0) {
            sum += tree[index];
            index -= index & -index;
        }
        return sum;
    }

    public void updateTree(int[] tree, int index, int value) {
        while (index < tree.length) {
            tree[index] += value;
            index += index & -index;
        }
    }

    public int[] createTree(int[] inputArray) {
        int[] bit = new int[inputArray.length + 1];
        for (int i = 1; i <= inputArray.length; i++) {
            updateTree(bit, i, inputArray[i-1]);
        }
        return bit;
    }

    public static void main(String[] args) {
        int[] testArr = {1, 4, 7, -3, 12, -1};
        FenwickTree ft = new FenwickTree();
        int[] bit = ft.createTree(testArr);
        assert 1 == ft.getPrefixSum(bit, 0);
        assert 5 == ft.getPrefixSum(bit, 1);
        assert 12 == ft.getPrefixSum(bit, 2);
        assert 9 == ft.getPrefixSum(bit, 3);
        assert 21 == ft.getPrefixSum(bit, 4);
        assert 20 == ft.getPrefixSum(bit, 5);

        // Update test
        testArr[2] = 2;
        // {1, 4, 2, -3, 12, -1}
        ft.updateTree(bit, 2, -5); // 7 + (-5) = 2
        assert 7 == ft.getPrefixSum(bit, 2);
        assert 15 == ft.getPrefixSum(bit, 5);

        System.out.println("Finished test");
    }
}
